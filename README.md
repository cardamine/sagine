Les questionnaires sont accessibles sur
[cardamine.frama.io/sagine](https://cardamine.frama.io/sagine/).  
Vous pouvez écrire `le mouvement` ou `l’énergie` sur la première page.  
Les mots de passe de la fin sont présents dans le fichier `public/variables.js`.